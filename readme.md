# How to use
- Copy this repository in your machine 
```
git clone https://giliweb@bitbucket.org/giliweb/slots.git
```
- rename `.env.example` to `.env`
- composer install
- from the command line:
```
php artisan game:spin [bet_amount]

```
- bet amount is optional, default is 1
- the board will be rendered in the command line and a the result will be dumped in JSON format
- few phpunit tests are also available to test the payout against specific boards: