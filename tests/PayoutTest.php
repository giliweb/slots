<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class PayoutTest extends TestCase
{



    public function test1(){
        $elements = ["J","K","J","J","K","K","Q","K","J","J","Q","J","10","10","J"];
        $payout = $this->testPayout($elements, 100);
        $this->assertEquals($payout->total_win, 20);
    }

    public function test2(){
        $elements = ["J","K","J","J","K","K","J","K","J","J","Q","J","10","10","J"];
        $payout = $this->testPayout($elements, 100);
        $this->assertEquals($payout->total_win, 220);
    }

    public function test3(){
        $elements = ["J","K","J","J","K","K","J","K","J","J","Q","J","J","10","J"];
        $payout = $this->testPayout($elements, 100);
        $this->assertEquals($payout->total_win, 1020);
    }

    /**
     * @group skip
     * @param array $elements
     * @param int $betAmount
     * @return object
     */
    public function testPayout(array $elements, int $betAmount){
        $board = new \App\Board();
        $board->symbols = \App\Game::SYMBOLS;
        $board->setElements($elements);
        $game = new \App\Game();
        $game->setBoard($board);

        $payout = \App\Http\Controllers\GameController::calculatePayout($betAmount, $game);
        return $payout;
    }
}
