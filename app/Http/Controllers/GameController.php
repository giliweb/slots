<?php

namespace App\Http\Controllers;

use App\Game;

class GameController extends Controller
{
    private $betAmount;

    public function spin(int $amount = Game::DEFAULT_BET_AMOUNT){
        // generate board
        $this->betAmount = $amount;
        $game = (new Game())->generateEmptyBoard();
        $game->getBoard()->populate()->cliRender();
        $payout = static::calculatePayout($this->betAmount, $game);
        return $payout;
    }

    public static function calculatePayout(int $betAmount = 100, Game $game){
        $payout = (object)[
            "board" => $game->getBoard()->getElements(),
            "paylines" => [],
            "bet_amount" => $betAmount,
            "total_win" => 0,
        ];

        $gameWinningMultipliers = $game->getWinningsMultiplier();

        foreach($game->getPaylines() as $payline){
            $line = array_intersect_key($game->getBoard()->getElements(), array_flip($payline));
            $line = array_values($line);
            //$occurrences = array_count_values($line);

            $occurrence = 0;
            do {
                $symbol = $line[$occurrence++];
            } while(isset($line[$occurrence]) && $symbol === $line[$occurrence]);


            if(isset($gameWinningMultipliers[$occurrence])){
                $payout->paylines[implode(' ', $payline)] = $occurrence;
                $payout->total_win += $gameWinningMultipliers[$occurrence] * $payout->bet_amount;
            }

        }
        return $payout;
    }

}
