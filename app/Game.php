<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model {

    const DEFAULT_BET_AMOUNT = 100;
    const ROWS = 3;
    const COLUMNS = 5;
    const SYMBOLS = [
        "9", "10", "J", "Q", "K", //"A", "C", "D", "M", "B"
    ];

    private $paylines = [
        [0, 3, 6, 9, 12],
        [1, 4, 7, 10, 13],
        [2, 5, 8, 11, 14],
        [0, 4, 8, 10, 12],
        [2, 4, 6, 10, 14],
    ];

    private $winningsMultipliers = [
        3 =>  0.2,
        4 =>  2.0,
        5 => 10.0,
    ];

    private $board;

    public function generateEmptyBoard(): Game {
        $this->board = Board::generateEmptyBoard(self::ROWS, self::COLUMNS, self::SYMBOLS);
        return $this;
    }

    public function getBoard(): Board {
        return $this->board;
    }

    public function setBoard($board) {
        $this->board = $board;
        return $this;
    }

    public function getPaylines(){
        return $this->paylines;
    }

    public function getWinningsMultiplier(){
        return $this->winningsMultipliers;
    }

}

