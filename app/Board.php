<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Board extends Model {

    private $elements;
    public $symbols;
    private $rows;
    private $columns;

    public function __construct(array $attributes = []){
        parent::__construct($attributes);
    }

    public static function generateEmptyBoard(int $rows = 3, int $columns = 5, array $symbols): Board {
        $elements = array_fill(0, $rows * $columns, NULL);
        $board = new static();
        $board->elements = $elements;
        $board->symbols = $symbols;
        $board->rows = $rows;
        $board->columns = $columns;
        return $board;
    }

    public function populate(){
        array_walk($this->elements, function(&$e, $k, $symbols){
            $e = array_random($symbols);
        }, $this->symbols);
        return $this;
    }

    public function getElements(){
        return $this->elements;
    }

    public function setElements($elements){
        $this->elements = $elements;
        return $this;
    }

    public function cliRender(){
        $output = "";
        for ($x = 0; $x < $this->rows; $x++){
            for ($y = 0; $y < $this->columns; $y++){
                $i = (($y * $this->rows) + $x);
                $symbol = $this->elements[$i];
                $symbol = strlen($symbol) === 1 ? " " . $symbol : $symbol;
                $output .= $symbol . " ";
            }
            $output .= "\n";
        }
        echo "$output\n";
    }
}

