<?php
namespace App\Console\Commands;
use App\Game;
use App\Http\Controllers\GameController;
use Illuminate\Console\Command;

class GameCommand extends Command {
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'game:spin {bet_amount?}';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Spin";
    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $betAmount = $this->argument('bet_amount') ? $this->argument('bet_amount') : Game::DEFAULT_BET_AMOUNT;
        $this->info("You bet $betAmount\n");
        $gameController = new GameController();
        echo json_encode($gameController->spin($betAmount * 100));
    }

}